package ro.orange;

public class Main {

    public static void main(String[] args) {
    	// object instantiation
	Sender snd = new Sender();
	ThreadedSend t1 = new ThreadedSend("Hi, Terry Williams", snd);
	ThreadedSend t2 = new ThreadedSend("We hope you enjoy the stay at our hotel!", snd);

	// start threads with run() method
	t1.start();
	t2.start();

	try {
	    t1.join();			// wait for t1 thread to finish
	    t2.join();			// wait for t2 thread to finish
    } catch (InterruptedException e) {
		System.out.println(e);	// not executed
    }

    }
}

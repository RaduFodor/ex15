package ro.orange;

public class ThreadedSend extends Thread {
    String msg;
    Sender sender;

    // constructor definition
    public ThreadedSend(String msg, Sender sender) {
        this.msg = msg;
        this.sender = sender;
    }

    // overwrite run method in Thread class
    public void run() {
        synchronized (this.sender){
            sender.send(this.msg);
        }
    }
}

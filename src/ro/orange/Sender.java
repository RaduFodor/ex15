package ro.orange;

public class Sender {
    public void send(String msg1) {                             // method expects a string
        System.out.println("Sending " + msg1);
        try {
            Thread.sleep(1000);                           // suspend thread for 1 sec
            }
            catch (InterruptedException e) {
            System.out.println("Thread interrupted!");
            }
        System.out.println(msg1 + "\nSent successfully!");
    }
}

